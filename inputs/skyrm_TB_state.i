
[Mesh]
  file = exodus_disk_r8_h1.e
[]

[MeshModifiers]
  [./centernodeset_1]
    type = AddExtraNodeset
    new_boundary = 'center_node_1'
    coord = '0.0 0.0 -0.5'
  [../]
  [./centernodeset_2]
    type = AddExtraNodeset
    new_boundary = 'center_node_2'
    coord = '0.0 0.0 0.5'
  [../]
  [./centernodeset_3]
    type = AddExtraNodeset
    new_boundary = 'center_node_3'
    coord = '0.0 0.0 0.166667'
  [../]
  [./centernodeset_4]
    type = AddExtraNodeset
    new_boundary = 'center_node_4'
    coord = '0.0 0.0 -0.166667'
  [../]
[]

[GlobalParams]
  len_scale = 1.0

  alpha1 = -0.09179 #room temp PTO
  alpha11 = 0.0706
  alpha111 = 0.0
  alpha12 = 0.1412
  alpha112 = 0.0
  alpha123 = 0.0

  G110 = 0.141
  G11_G110 = 1.0 #this is here to somehow prevent P_z "ringing" problems on the side...
  G12_G110 = 0.0 #perhaps this allows for divP =0?
  G44_G110 = 1.0
  G44P_G110 = 1.0

  polar_x = polar_x
  polar_y = polar_y
  polar_z = polar_z

  potential_int = potential_int
[]

[Functions]
  [./parsed_function]
    type = ParsedFunction
    value = '0.0005*(0.7-0.0625*(x^2+y^2)^(0.5)+0.0323*(x^2+y^2)-0.0156*(x^2+y^2)^(1.5)+0.0013*(x^2+y^2)^2)'
  [../]
  [./parsed_function2]
    type = ParsedFunction
    value = '-0.0005*sin(2*3.14159*y/8)'
  [../]
  [./parsed_function3]
    type = ParsedFunction
    value = '-0.0005*sin(2*3.14159*x/8)'
  [../]
[]

[Variables]
  [./polar_x]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = RandomIC
      min = -0.5e-6
      max = 0.5e-6
    [../]
    #[./InitialCondition]
    #  type = FunctionIC
    #  function = parsed_function2
    #[../]
  [../]
  [./polar_y]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = RandomIC
      min = -0.5e-6
      max = 0.5e-6
    [../]
    #[./InitialCondition]
    #  type = FunctionIC
    #  function = parsed_function3
    #[../]
  [../]
  [./polar_z]
    order = FIRST
    family = LAGRANGE
    [./InitialCondition]
      type = RandomIC
      min = -0.5e-6
      max = 0.5e-6
    [../]
    #[./InitialCondition]
    #  type = FunctionIC
    #  function = parsed_function
    #[../]
  [../]

  [./potential_int]
    order = FIRST
    family = LAGRANGE
  [../]
[]


[Kernels]
  #Bulk energy density
  [./bed_x]
    type = BulkEnergyDerivativeSixth
    variable = polar_x
    component = 0
  [../]
  [./bed_y]
    type = BulkEnergyDerivativeSixth
    variable = polar_y
    component = 1
  [../]
  [./bed_z]
    type = BulkEnergyDerivativeSixth
    variable = polar_z
    component = 2
  [../]

  ##Wall energy penalty
  [./walled_x]
    type = WallEnergyDerivative
    variable = polar_x
    component = 0
  [../]
  [./walled_y]
    type = WallEnergyDerivative
    variable = polar_y
    component = 1
  [../]
  [./walled_z]
     type = WallEnergyDerivative
     variable = polar_z
     component = 2
  [../]

  #[./polar_xEstrong] #these terms send P to infinity at singularities on the mesh.
  #   type = PolarElectricEStrong
  #   variable = polar_x
  #[../]
  #[./polar_yEstrong]
  #   type = PolarElectricEStrong
  #   variable = polar_y
  #[../]
  #[./polar_zEstrong]
  #   type = PolarElectricEStrong
  #   variable = polar_z
  #[../]

  [./anis_x]
    type = AnisotropyEnergy
    variable = polar_x
    component = 0
    K = 0.0565 #minus means IN-PLANE
  [../]
  [./anis_y]
    type = AnisotropyEnergy
    variable = polar_y
    component = 1
    K = 0.0565
  [../]

  ##Electrostatics
  [./polar_x_electric_E]
     type = PolarElectricEStrong
     variable = potential_int
  [../]
  [./FE_E_int]
     type = Electrostatics
     variable = potential_int
     block = '1'
     permittivity = 0.08854187
  [../]

  [./polar_electric_px]
     type = PolarElectricPStrong
     variable = polar_x
     component = 0
  [../]
  [./polar_electric_py]
     type = PolarElectricPStrong
     variable = polar_y
     component = 1
  [../]
  #[./polar_electric_pz]
  #   type = PolarElectricPStrong
  #   variable = polar_z
  #   component = 2
  #[../]

  [./depol_z]
    type = DepolEnergy
    permitivitty = 0.008854187
    lambda = 0.0007 #this needs to be adjusted as a function of thickness. Can we remove the ringing with G11 = 0?
    variable = polar_z
    avePz = avePz
  [../]


    ##Time dependence
    [./polar_x_time]
       type = TimeDerivativeScaled
       variable=polar_x
      time_scale = 1.0
    [../]
    [./polar_y_time]
       type = TimeDerivativeScaled
       variable=polar_y
      time_scale = 1.0
    [../]
    [./polar_z_time]
       type = TimeDerivativeScaled
       variable = polar_z
      time_scale = 1.0
    [../]
[]


[BCs]
  #[./center_pol_x]
  #  type = DirichletBC
  #  variable = 'polar_x'
  #  value = 0.0
  #  boundary = 'center_node_1 center_node_2 center_node_3 center_node_4'
  #[../]
  #[./center_pol_y]
  #  type = DirichletBC
  #  variable = 'polar_y'
  #  value = 0.0
  #  boundary = 'center_node_1 center_node_2 center_node_3 center_node_4'
  #[../]

 # [./center_pol_z]
 #   type = DirichletBC
 #   variable = 'polar_z'
 #   value = 0.65
 #   boundary = 'center_node_1 center_node_2 center_node_3 center_node_4'
 # [../]

  #[./side_neumann_x]
  #  variable = 'polar_x'
  #  type = NeumannBC
  #  value = 0.0
  #  boundary = '1'
  #[../]
  #[./side_neumann_y]
  #  variable = 'polar_y'
  #  type = NeumannBC
  #  value = 0.0
  #  boundary = '1'
  #[../]
  #[./side_Neumann_z]
  #  variable = 'polar_z'
  #  type = NeumannBC
  #  value = 0.0
  #  boundary = '1'
  #[../]
[]



[Postprocessors]
   [./avePz]
     type = ElementAverageValue
     variable = polar_z
     execute_on = 'initial linear nonlinear timestep_begin timestep_end'
   [../]
   [./Fbulk]
      type = BulkEnergy
      execute_on = 'timestep_end'
    [../]
    [./Fwall]
      type = WallEnergy
      execute_on = 'timestep_end'
    [../]
[]

[Preconditioning]
  [./smp]
    type = SMP
    full = true
    petsc_options_iname = '-ksp_gmres_restart -snes_atol -snes_rtol -ksp_rtol -pc_type'
    petsc_options_value = '    250              1e-10      1e-8      1e-6      bjacobi   '
  [../]
[]

[Executioner]
  type = Transient
    [./TimeStepper]
    type = IterationAdaptiveDT
    dt = 0.2
    #iteration_window = 3
    optimal_iterations = 6 #should be 5 probably
    growth_factor = 1.4
    linear_iteration_ratio = 1000
    cutback_factor =  0.9
[../]
  solve_type = 'PJFNK'       #"PJFNK, JFNK, NEWTON"
  scheme = 'implicit-euler'   #"implicit-euler, explicit-euler, crank-nicolson, bdf2, rk-2"
  dtmin = 1e-13
  dtmax = 1.0
[]

[Outputs]
  print_linear_residuals = true
  print_perf_log = true
  [./out]
    type = Exodus
    execute_on = 'timestep_end'
    file_base = out_skyrm_parsed
    elemental_as_nodal = true
  [../]
[]
