/**
   This file is part of FERRET, an add-on module for MOOSE

   FERRET is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

   For help with FERRET please contact J. Mangeri <mangeri@fzu.cz>
   and be sure to track new changes at bitbucket.org/mesoscience/ferret

*/

#ifndef MIEMAGFIELDREALS_H
#define MIEMAGFIELDREALS_H

#include "AuxKernel.h"
#include "RankTwoTensor.h"

//Forward declarations
class MieMagFieldReals;

template<>
InputParameters validParams<MieMagFieldReals>();


class MieMagFieldReals : public AuxKernel
{
public:
  MieMagFieldReals(const InputParameters & parameters);

  virtual ~MieMagFieldReals() {}

protected:
  virtual Real computeValue();
  const Real _a, _omega, _c, _epsilonI, _sigmaI, _epsilonII, _sigmaII, _L, _nh, _order, _component;
private:

};

#endif // MIEMAGFIELDREALS_H
